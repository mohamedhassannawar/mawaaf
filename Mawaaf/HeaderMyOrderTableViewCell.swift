//
//  HeaderMyOrderTableViewCell.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 7/5/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//


import UIKit

class HeaderMyOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
