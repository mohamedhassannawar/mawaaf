//
//  CodableStructs.swift
//  Mawaaf
//
//  Created by Mac on 9/14/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//


struct LinesData : Decodable {
    var id:Int?
    var sourceCity : CityData?
    var destinationCity:CityData?
    var sourceCheckpoints:[CityData]?
    var destinationCheckpoints:[CityData]?
    var trips: [TripData]?
    var pricePerPerson: Int?
    var totalWaitingTime: Int?
}
struct CityData : Decodable {
    var id:Int?
    var cityName : String?
    var address:String?
    var name : String?
    var waitTime:Int?
}
struct TripData : Decodable {
    var id:Int?
    var startTime : String?
    var status:String?
    var availableSeats:Int?
    var weekly : Bool?
    var line : Line?
    var captain : CaptainData?
}
struct CaptainData : Decodable {
    var id:Int?
    var licenseNumber : String?
    var licenseType:String?
    var coreUser:UserDetails?
    var car : CarData?
}
struct CarData : Decodable {
    var id:Int?
    var ownerName : String?
    var agentName:String?
    var passengerLoad:Int?
    var plateNumber : String?
    var model:String?
    var carType : String?
}

struct   TempToken: Decodable {
    var temp_token : String?
}


struct coreUser : Decodable{
    var email : String
}

struct TheData : Decodable {
    var id:Int?
    var sourceCheckpoint:CityData?
    var destinationCheckpoint:CityData?
    var trip: TripData?
    var price : Int?
    var seatsNumber : Int?
}
struct Line : Decodable {
    var id : Int?
    var totalWaitingTime : Int?
}

struct requestedTripData : Decodable {
    var id : Int?
}

