//
//  ViewController.swift
//  Rezerva
//
//  Created by Mohamed Nawar on 5/4/19.
//


import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}
    
    private let url = "https://mawaaf.com/"
    
    public func getHeader() -> [String: String]{
        let header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \(userData.Instance.token ?? "")"  ,
             "Content-Type":"application/json",
            "X-LOCALE" : "ar"
        ]
        return header
    }
    //    \(userData.Instance.token ?? "")
    public func registeration() -> String{
        return url + "api/customers/register"
    }
    public func requestTrip() -> String{
        return url + "api/requests"
    }
    public func sendMessage() -> String{
        return url + "api/complains"
    }
    
    public func userCheckout() -> String{
        return url + "checkout?lang=\(L102Language.currentAppleLanguage())"
    }
    public func login() -> String{
        return url + "login"
    }
    public func lines() -> String{
        return url + "api/lines"
    }
    public func deleteTrip(id : String) -> String{
        return url + "api/requests/\(id)"
    }
    public func loginFacebook() -> String{
        return url + "login/facebook"
    }
    public func loginTwitter() -> String{
        return url + "login/twitter"
    }
    public func getBranches() -> String{
        return url + "branches"
    }
    public func getPastTrips() -> String{
        return url + "api/requests?past=true"
    }

    public func forgetPassword1() -> String{
        return url + "forget_password"
    }
    public func forgetPassword2() -> String{
        return url + "forget_password2"
    }
    public func forgetPassword3() -> String{
        return url + "forget_password3"
    }
    public func UserShowProfile() -> String{
        return url + "profile"
    }
    public func UpdateProfile() -> String{
        return url + "edit_profile"
    }
    public func contactUs() -> String{
        return url + "contact"
    }
    public func aboutUs() -> String{
        return url + "about?lang=\(L102Language.currentAppleLanguage())"
    }
    public func addComment() -> String{
        return url + "add_comment"
    }
    public func allCommentOfProduct(id:Int) -> String{
        return "https://www.scarfi-hijabs.com/wp-json/wp/v2/comments?post=\(id)"
    }
    public func addToFavorite() -> String{
        return url + "add_favorite"
    }
    public func removeFromFavorite() -> String{
        return url + "remove_favorite"
    }
    public func showFavorite() -> String{
        return url + "all_favorite"
    }
    public func showFilterData() -> String{
        return url + "filter_options?lang=\(L102Language.currentAppleLanguage())"
    }
    public func getAreasData() -> String{
        return url + "shipping_areas"
    }
    public func enterCoupon() -> String {
        //        return url + "/check_coupon"
        return "https://www.scarfi-hijabs.com/wp-json/wc/v2/check_coupon"
    }
    public func FilterData(colors:String,sizes:String,category:String,min:String,max:String,searchTerm:String,lan:String) -> String{
        return url + "filter/?colors=\(colors)&sizes=\(sizes)&category=\(category)&min_price=\(min)&max_price=\(max)&search_term=\(searchTerm)&?lang=\(L102Language.currentAppleLanguage())"
    }
    public func productsCategories() -> String{
        return url + "products/categories/?consumer_key=ck_881fd7df4eaf26bc6d4462a099af0cf661281cba&consumer_secret=cs_c3947d2e8f877847e17027b40670e084e5edec80&lang=\(L102Language.currentAppleLanguage())"
    }
    public func myOrders() -> String{
        return url + "my_orders"
    }
    public func Showproduct(id:Int) -> String{
        return url + "products/\(id)?consumer_key=ck_881fd7df4eaf26bc6d4462a099af0cf661281cba&consumer_secret=cs_c3947d2e8f877847e17027b40670e084e5edec80&lang=\(L102Language.currentAppleLanguage())"
    }
    public func ShowSectionproducts(id:Int) -> String{
        return url + "products?consumer_key=ck_881fd7df4eaf26bc6d4462a099af0cf661281cba&consumer_secret=cs_c3947d2e8f877847e17027b40670e084e5edec80&lang=\(L102Language.currentAppleLanguage())&per_page=10&page=1&category=\(id)"
    }
    
}
