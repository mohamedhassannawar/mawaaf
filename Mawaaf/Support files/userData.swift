//
//  ViewController.swift
//  Rezerva
//
//  Created by Mohamed Nawar on 5/4/19.
//

import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var firstUse:String?
    
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")

        token = ""
    }
    
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifier")
    }
    
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "firstUse") {
            self.firstUse = String(data)
        }
    }
}
struct CurrentUser:Decodable {
    var data: UserDetails?
}
struct UserDetails : Decodable {
    var id:Int?
    var name : String?
    var email:String?
    var mobile:String?
    var type:String?
    var avatar: String?
    var address: String?
    var gender: String?
    var verified: Bool?
    var avatar_conversions: AvatarConversions?
    var firstName : String?
    var lastName : String?
    var mobileNumber : String?

}
struct AvatarConversions:Decodable {
    var small: String?
    var medium: String?
    var large: String?
    var original: String?
}




