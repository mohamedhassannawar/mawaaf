//
//  MyOrdersViewController.swift
//  Scarfi
//
//  Created by iMac on 11/28/18.
//  Copyright © 2018 iMac. All rights reserved.
//


import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class BusLocationVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var OrdersTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        OrdersTableView.reloadData()
        OrdersTableView.delegate = self
        OrdersTableView.dataSource = self
        OrdersTableView.separatorStyle = .none
    }
    

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell1 = UITableViewCell()
        if indexPath.row == 0 {
            let cell11 = OrdersTableView.dequeueReusableCell(withIdentifier: "HeaderOrdersCell", for: indexPath) as! HeaderMyOrderTableViewCell
            return cell11
        }
        if indexPath.row == 4 {
            cell1 = OrdersTableView.dequeueReusableCell(withIdentifier: "FooterOrdersCell", for: indexPath)
            return cell1
            
        }
        cell1 = OrdersTableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath)
        return cell1
    }
}
