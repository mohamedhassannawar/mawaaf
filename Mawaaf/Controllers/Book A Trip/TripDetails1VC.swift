//
//  TripDetails1VC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
var globalPrice = -1
var globalSeats = 1

class TripDetails1VC: UIViewController {
    
    @IBOutlet weak var bookButton : UIButton!
    @IBOutlet weak var sourceCityLabel : UILabel!
    @IBOutlet weak var sourceCheckPointLabel : UILabel!
    @IBOutlet weak var desCityLabel : UILabel!
    @IBOutlet weak var desCheckPointLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var lineLabel : UILabel!
    @IBOutlet weak var numberOfSeatsTF : UITextField!
    
    var lines = [LinesData]()
    var index = -1
    var price = 0
    var flag = 1
    var requestedTrip = requestedTripData()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberOfSeatsTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        globalPrice = (lines.first?.pricePerPerson!)!
        print(lines)
        print(index)
        desCheckPointLabel.text = lines.first?.destinationCity?.address
        desCityLabel.text = lines.first?.destinationCity?.cityName
        sourceCheckPointLabel.text = lines.first?.sourceCity?.address
        sourceCityLabel.text = lines.first?.sourceCity?.cityName
        if let price = lines.first?.pricePerPerson {
            self.price = price
            priceLabel.text = "\(price)"
        }
        if let line = lines.first?.trips?[index].id {
            self.lineLabel.text = "# \(line)"
        }
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    @IBAction func BookButtonPressed (_ sender : Any){
        requestTrip()
//        let navVC = AppStoryboard.Main.viewController(viewControllerClass: SuccessfulBookingVC.self)
//        navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.navigationController!.present(navVC, animated: true, completion: nil)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        print (textField.text ?? "no")
        
        let textAsNumber = Int(textField.text!)
        if let theText = textAsNumber{
            guard theText > 0 && theText < 50 else {
                let lockString = NSLocalizedString("please enter a valid seats number", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                flag = 0
                return
            }
            self.priceLabel.text = "\(theText * self.price)"
            globalPrice = theText * self.price
            globalSeats = theText
            flag = 1
        } else {
            let lockString = NSLocalizedString("please enter a valid seats number", comment: "")
            HUD.flash(.label(lockString), delay: 1.0)
            flag = 0
        }
        
    }
    private func requestTrip(){
        if self.flag == 0  {
            HUD.flash(.label(NSLocalizedString("please enter a valid seats number", comment: "")), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        
        let data : [String: Any] = [
            "trip": [
                "id": (lines.first?.trips?[index].id)!
            ],
            "sourceCheckpoint": [
                "id": (lines.first?.sourceCheckpoints?.first?.id!)!
            ],
            "destinationCheckpoint": [
                "id": (lines.first?.destinationCheckpoints?.first?.id!)!
            ],
            "customer": [
                "id": Int((UserDefaults.standard.string(forKey: "id"))!)!
            ],
            "price": Int(self.priceLabel.text!)!,
            "seatsNumber": Int(self.numberOfSeatsTF.text!)!
        ]
        print (data)
        print (APIs.Instance.requestTrip())
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.requestTrip(), method: .post, parameters: data , encoding: JSONEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    print (APIs.Instance.requestTrip())
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        print ("successs")
                        self.requestedTrip = try JSONDecoder().decode(requestedTripData.self, from: response.data!)
                        print (self.requestedTrip.id)
                        let value = self.requestedTrip.id
                        UserDefaults.standard.set(value!, forKey: "requestedTripId")
                        let navVC = AppStoryboard.Main.viewController(viewControllerClass: SuccessfulBookingVC.self)
                        navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        self.navigationController!.present(navVC, animated: true, completion: nil)
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
}


