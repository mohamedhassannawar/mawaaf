//
//  TripDetailsVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import FCAlertView
import SideMenu
import PKHUD
import Alamofire

class TripDetailsVC: UIViewController {
    @IBOutlet weak var OrdersTableView: UITableView!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var lineLabel : UILabel!
    @IBOutlet weak var driverNameLabel : UILabel!
    @IBOutlet weak var driverNumberLabel : UILabel!
    @IBOutlet weak var plateNumberLabel : UILabel!
    @IBOutlet weak var numberOfSeatsLabel : UILabel!
    
    
    let alert = FCAlertView()
    var numberOfRows = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "inTrip")
        print (globalLines.count)
        print (globalIndex)
        numberOfRows = (globalLines.first?.destinationCheckpoints?.count ?? 0) + (globalLines.first?.sourceCheckpoints?.count ?? 0)
        
        print (numberOfRows)
        self.priceLabel.text = "\(globalPrice)"
        if let line = globalLines.first?.trips?[globalIndex].id {
            self.lineLabel.text = "# \(line)"
        }
        self.driverNameLabel.text = globalLines.first?.trips?[globalIndex].captain?.coreUser?.firstName!
        self.driverNumberLabel.text = globalLines.first?.trips?[globalIndex].captain?.coreUser?.mobileNumber!
        self.plateNumberLabel.text = globalLines.first?.trips?[globalIndex].captain?.car?.plateNumber!
        self.numberOfSeatsLabel.text = "\(globalSeats)"
        
        OrdersTableView.delegate = self
        OrdersTableView.dataSource = self
        OrdersTableView.separatorStyle = .none
        alert.delegate = self
        //setupSideMenu()
        SideMenuManager.default.menuFadeStatusBar = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

        
    }
    
    @IBAction func cancilTripButtonPressed(_ sender: Any) {

        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        alert.hideDoneButton = true
        alert.showAlert(inView: self,
                        withTitle:"Cancel Trip",
                        withSubtitle:"Are You sure you want to cancel your trip?",
                        withCustomImage:nil,
                        withDoneButtonTitle:nil,
                        andButtons:["Yes", "No"])
    }
    
    func setupSideMenu (){
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "side") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuFadeStatusBar = false
    }
    private func deleteTrip(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(header)
        let deletedTripId = Int(UserDefaults.standard.integer(forKey: "requestedTripId"))
        print (deletedTripId)
        let url = APIs.Instance.deleteTrip(id: "\(deletedTripId)")
        print (url)
        Alamofire.request(url, method: .delete, parameters: nil , encoding: JSONEncoding.default, headers: header).responseJSON {
            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        print ("suceeeeesssss deleteeeddd")
                        print ( "keep")
                        
                        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "whereToGoNav")
                        self.navigationController?.present(navVC!, animated: true, completion: nil)

                    }catch{
                        print(error.localizedDescription)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }

}

extension TripDetailsVC : FCAlertViewDelegate{
    
    func fcAlertView(_ alertView: FCAlertView, clickedButtonIndex index: Int, buttonTitle title: String) {
        
        if title == "Yes" {
            UserDefaults.standard.set(false, forKey: "inTrip")
            self.deleteTrip()
        }else if title == "No"{
            print (" remove")
            self.alert.dismiss()
        }
    }

}

extension TripDetailsVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var cell1 = UITableViewCell()
        if indexPath.row == 0 {
            let cell11 = OrdersTableView.dequeueReusableCell(withIdentifier: "HeaderOrdersCell", for: indexPath) as! HeaderMyOrderTableViewCell
            cell11.addressLabel.text = globalLines.first?.sourceCheckpoints?.first?.address
            cell11.cityLabel.text = globalLines.first?.sourceCheckpoints?.first?.name
            return cell11
        }
        if indexPath.row == numberOfRows-1 {
           let cell1 = OrdersTableView.dequeueReusableCell(withIdentifier: "FooterOrdersCell", for: indexPath) as! HeaderMyOrderTableViewCell
            let half = numberOfRows/2
            cell1.addressLabel.text = globalLines.first?.destinationCheckpoints![half - 1].address
            cell1.cityLabel.text = globalLines.first?.destinationCheckpoints![half - 1].name
            return cell1
            
        }
       let cell1 = OrdersTableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! HeaderMyOrderTableViewCell
        let sourceCount = globalLines.first?.sourceCheckpoints!.count
        
        if indexPath.row < sourceCount! {
            cell1.addressLabel.text = globalLines.first?.sourceCheckpoints?[indexPath.row].address
            cell1.cityLabel.text = globalLines.first?.sourceCheckpoints?[indexPath.row].name
        }else {
            cell1.addressLabel.text = globalLines.first?.destinationCheckpoints?[indexPath.row - sourceCount!].address
            cell1.cityLabel.text = globalLines.first?.destinationCheckpoints?[indexPath.row - sourceCount!].name
            
        }

        return cell1
    }
}
