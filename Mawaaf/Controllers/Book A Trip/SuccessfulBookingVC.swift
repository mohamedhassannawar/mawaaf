//
//  SuccessfulBookingVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

    import UIKit

    class SuccessfulBookingVC: UIViewController {
        
        @IBOutlet var backGround: UIView!

        override func viewDidLoad() {
            super.viewDidLoad()
            
            //let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
            //mytapGestureRecognizer.numberOfTapsRequired = 1
            //self.backGround.addGestureRecognizer(mytapGestureRecognizer)
            //userData.Instance.fetchUser()
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.barTintColor = .clear
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .white

            
        }
        
        
        @IBAction func DoneButtonPressed(_ sender: Any) {
            print ("hi")
            //self.dismiss(animated: true, completion: nil)
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "tripDetailsNAV")
            self.present(navVC!, animated: true, completion: nil)

        }
        
        @IBAction func editBooking(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        
}
