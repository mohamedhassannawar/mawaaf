//
//  EnterWhereToGoVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/29/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import FCAlertView
import PKHUD

class EnterWhereToGoVC: UIViewController{
    
    
    
    var lines = [LinesData](){
        didSet{
            fromTxt.text = lines[0].sourceCity?.cityName ?? ""
            fromCheckPointTxt.text = lines[0].sourceCheckpoints?[0].name ?? ""
            toTxt.text = lines[selectedFromId].destinationCity?.cityName ?? ""
            toCheckPointTxt.text = lines[0].destinationCheckpoints?[0].name ?? ""
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            dateTxt.text = result
            fromTxtpicker.reloadAllComponents()
        }
    }
    var selectedFromId = 0
    @IBOutlet weak var fromTxt: UITextField!
    @IBOutlet weak var fromCheckPointTxt: UITextField!
    @IBOutlet weak var toTxt: UITextField!
    @IBOutlet weak var toCheckPointTxt: UITextField!
    @IBOutlet weak var dateTxt: UITextField!
    
    var fromTxtpicker = UIPickerView()
    var fromCheckPointTxtpicker = UIPickerView()
    var toTxtpicker = UIPickerView()
    var toCheckPointTxtpicker = UIPickerView()
    var datePicker = UIDatePicker()
    let date = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.string(forKey: "id"))
        getLines()
        datePicker.datePickerMode = .date
        dateTxt.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        
        let arrOfPickers = [fromTxtpicker , fromCheckPointTxtpicker  ,toCheckPointTxtpicker]
        for picker in arrOfPickers {
            picker.delegate = self
            picker.dataSource = self
        }
        fromTxt.inputView = fromTxtpicker
        fromCheckPointTxt.inputView = fromCheckPointTxtpicker
        //        toTxt.inputView = toTxtpicker
        toCheckPointTxt.inputView = toCheckPointTxtpicker
        
        //setupSideMenu()
        SideMenuManager.default.menuFadeStatusBar = false
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    
    @objc func dateChanged (datePicker : UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateTxt.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: BusLineVC.self)
        
        let finalURL =           "https://www.mawaaf.com/api/lines?sourceCity.cityName=\(fromTxt.text!.fixedArabicURL ?? "")&destinationCity.cityName=\(toTxt.text!.fixedArabicURL ?? "")&sourceCheckpoints.name=\(fromCheckPointTxt.text!.fixedArabicURL ?? "")&destinationCheckpoints.name=\(toCheckPointTxt.text!.fixedArabicURL ?? "")&trips.startTime=\(dateTxt.text!.fixedArabicURL ?? "")"
        print (finalURL)
        
        navVC.searchUrl = finalURL
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    private func getLines(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(header)
        print(APIs.Instance.lines())
        Alamofire.request(APIs.Instance.lines(), method: .get, parameters: nil , encoding: JSONEncoding.default, headers: header).responseJSON {
            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.lines = try JSONDecoder().decode([LinesData].self, from: response.data!)
                        if self.lines.count == 0 {
                            self.makeDoneAlert(title: "No Trips right now , please come on Later.", SubTitle: "", Image:  UIImage())
                        } 
                    }catch{
                        print(error.localizedDescription)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    
}


extension EnterWhereToGoVC :  UIPickerViewDelegate , UIPickerViewDataSource  {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == fromTxtpicker {
            return lines.count ?? 0
        }
        if pickerView == fromCheckPointTxtpicker {
            return lines[selectedFromId].sourceCheckpoints?.count ?? 0
        }
        if pickerView == toCheckPointTxtpicker {
            return lines[selectedFromId].destinationCheckpoints?.count ?? 0
        }
        
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if fromTxt.isEditing{
            fromTxt.text = lines[row].sourceCity?.cityName ?? ""
            selectedFromId = row
            fromCheckPointTxt.text = lines[selectedFromId].sourceCheckpoints?[0].name ?? ""
            toTxt.text = lines[selectedFromId].destinationCity?.cityName ?? ""
            fromCheckPointTxtpicker.reloadAllComponents()
            toCheckPointTxtpicker.reloadAllComponents()
        }else if fromCheckPointTxt.isEditing{
            fromCheckPointTxt.text = lines[selectedFromId].sourceCheckpoints?[row].name ?? ""
            toTxt.text = lines[selectedFromId].destinationCity?.cityName ?? ""
            toCheckPointTxtpicker.reloadAllComponents()
        }else if toTxt.isEditing{
            toTxt.text = lines[selectedFromId].destinationCity?.name
        }else if toCheckPointTxt.isEditing{
            toCheckPointTxt.text = lines[selectedFromId].destinationCheckpoints?[row].name ?? ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == fromTxtpicker {
            return lines[row].sourceCity?.cityName ?? ""
        }
        if pickerView == fromCheckPointTxtpicker {
            return lines[selectedFromId].sourceCheckpoints?[row].name ?? ""
        }
        if pickerView == toCheckPointTxtpicker {
            return lines[selectedFromId].destinationCheckpoints?[row].name ?? ""
        }
        return ""
    }
    
    
    
    func setupSideMenu (){
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "side") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuFadeStatusBar = false
    }
    
    
}

extension String {
    var fixedArabicURL: String?  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlHostAllowed))
    } }
