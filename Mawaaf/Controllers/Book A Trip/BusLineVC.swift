//
//  BusLineVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/29/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

var globalIndex = -1
var globalLines = [LinesData]()


class BusLineVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var lines = [LinesData](){
        didSet{
            self.tableView.reloadData()
            globalLines = self.lines
        }
    }
    var searchUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        tableView.delegate = self
        tableView.dataSource = self
        print (self.lines)
        getLinesSearch(url : searchUrl)
        
        self.tableView.separatorStyle = .none
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    private func getLinesSearch(url : String){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        
        print (url)
        
        //print (finalURL)
        Alamofire.request(url, method: .get, parameters: nil , encoding: JSONEncoding.default, headers: header).responseData {            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                print (APIs.Instance.login())
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        print("succceeessss")
                        self.lines = try JSONDecoder().decode([LinesData].self, from: response.data!)
                        if self.lines.count == 0 {
                            self.makeDoneAlert(title: "no Trips in this date", SubTitle: "please chosse another date", Image: UIImage())
                        }
                        
                    }catch{
                        print(error.localizedDescription)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    
    
}
extension BusLineVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(lines.count ?? 0)
        return lines.first?.trips?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusLineCell", for: indexPath) as! LineTableViewCell
        //cell.dateLabel.text = lines.first?.trips![indexPath.row].startTime
        //cell.dayLabel.text = ""
        cell.desCheckPointLabel.text = lines.first?.destinationCity?.address
        cell.desCityLabel.text = lines.first?.destinationCity?.cityName
        cell.sourceCheckPointLabel.text = lines.first?.sourceCity?.address
        cell.sourceCityLabel.text = lines.first?.sourceCity?.cityName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: TripDetails1VC.self)
        navVC.index = indexPath.row
        navVC.lines = self.lines
        globalIndex = indexPath.row
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}

