//
//  ConfirmPasswordVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class ConfirmPasswordVC: UIViewController {
    var token = String()
    @IBOutlet weak var confirmPasswordTxt: HoshiTextField!
    @IBOutlet weak var passwordTxt: HoshiTextField!
    @IBOutlet weak var backgroundImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

    }
    private func forgetPasswordStep3(){
        if self.passwordTxt.text == "" || self.confirmPasswordTxt.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter your Password and Confirm Password", comment: "")), delay: 1.0)
            return
        }
        
        if self.passwordTxt.text != self.confirmPasswordTxt.text {
            HUD.flash(.label(NSLocalizedString("Password not identical", comment: "")), delay: 1.0)
            return
            
        }
        let header = ["temp-token" : "\(token)"]
        
        let par = ["password": passwordTxt.text!] as [String : String]
        print(par)
        
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.forgetPassword3() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  #imageLiteral(resourceName: "danger"))
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        
                        print("successsss")
                        let navVC = AppStoryboard.Main.viewController(viewControllerClass: loginVC.self)
                        self.navigationController?.pushViewController(navVC, animated: true)
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                    
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

    @IBAction func Send(_ sender: Any) {
        //forgetPasswordStep3()
        let navVc = self.storyboard?.instantiateViewController(withIdentifier: "loginNavigation")
        self.navigationController?.present(navVc!, animated: true, completion: nil)
    }
}
