//
//  ChangePasswordVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//



    import UIKit
    import Alamofire
    import PKHUD
    import FCAlertView
    import TextFieldEffects
    class ChangePasswordVC: UIViewController {
        var shouldBack = String()
        
        @IBOutlet weak var confirmPasswordTxt: HoshiTextField!
        @IBOutlet weak var newpasswordTxt: HoshiTextField!
        @IBOutlet weak var oldpasswordTxt: HoshiTextField!
        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.barTintColor = .clear
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.barTintColor = .clear
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .white

        }
        
        
        private func userSignUp(){
            if self.confirmPasswordTxt.text == "" || self.oldpasswordTxt.text == "" || self.newpasswordTxt.text == ""  {
                HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
                return
            }
            if self.newpasswordTxt.text != self.confirmPasswordTxt.text {
                HUD.flash(.label(NSLocalizedString("Password not identical", comment: "")), delay: 1.0)
                return
                
            }
            let header = APIs.Instance.getHeader()
            
            let par = ["email": oldpasswordTxt.text! , "phone": newpasswordTxt.text!.replacedArabicDigitsWithEnglish, "password": confirmPasswordTxt.text!.replacedArabicDigitsWithEnglish ] as [String : String]
            print(par)
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                print(response)
                switch(response.result) {
                case .success(let value):
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    print(temp)
                    if temp >= 300 {
                        print("errorrrr")
                        do {
                            let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  #imageLiteral(resourceName: "danger"))
                            print(err.detail)
                        }catch{
                            print("errorrrrelse")
                            
                        }
                    }else{
                        
                        do {
                            userData.Instance.saveUser(data: response.data!)
                            userData.Instance.fetchUser()
                            
                            print("successsss")
                            //                            self.navigationController?.popViewController(animated: true)
                            //                            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                            
                        }catch{
                            HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
            
        }
        
 
        
        @IBAction func SignUp(_ sender: Any) {
            userSignUp()
        }
}

