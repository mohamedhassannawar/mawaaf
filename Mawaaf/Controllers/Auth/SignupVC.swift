//
//  SignupVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class SignupVC: UIViewController {
    var shouldBack = String()
    
    @IBOutlet weak var passwordTxt: HoshiTextField!
    @IBOutlet weak var phoneTxt: HoshiTextField!
    @IBOutlet weak var emailTxt: HoshiTextField!
    @IBOutlet weak var userNameTxt: HoshiTextField!
    @IBOutlet weak var agreeButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        agreeButton.isSelected = true
        agreeButton.cornerRadius = 8
    }
    
    @IBAction func SignUp(_ sender: Any) {

        userSignUp()
    }
    
    
    @IBAction func terms(_ sender: Any) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: Privacy_PolicyVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func agreeButoonPressed (_ sender: Any){
        agreeButton.isSelected = !agreeButton.isSelected
        if agreeButton.isSelected {
            agreeButton.setImage(UIImage(named: "tick-1"), for: .normal)
        }else {
            agreeButton.setImage(UIImage(named: ""), for: .normal)
        }
    }
    
    private func userSignUp(){
        if self.emailTxt.text == "" ||  self.passwordTxt.text == "" || self.phoneTxt.text == "" || self.userNameTxt.text == ""  {
            HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
            return
        }
        if self.agreeButton.isSelected == false {
            HUD.flash(.label(NSLocalizedString("Please Agree for Privacy and Policy", comment: "")), delay: 1.0)
        }
        let header = APIs.Instance.getHeader()
        
        let data : [String: Any] = [
            "coreUser": [
                "username": userNameTxt.text!,
                "email": emailTxt.text!,
                "password": passwordTxt.text!.replacedArabicDigitsWithEnglish,
                "firstName": "",
                "lastName": "",
                "mobileNumber": phoneTxt.text!.replacedArabicDigitsWithEnglish
            ]
        ]
        
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: data , encoding: JSONEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("token",userData.Instance.token)
                        print("successsss")
                        let navVC = AppStoryboard.Main.viewController(viewControllerClass: EnterWhereToGoVC.self)
                        self.navigationController?.pushViewController(navVC, animated: true)
                        
             
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }

 
}

