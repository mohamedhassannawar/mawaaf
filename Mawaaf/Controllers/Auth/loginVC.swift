//
//  loginVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
import JWTDecode


class loginVC: UIViewController {
    var shouldBack = String()
    var signinTag = Int()
    @IBOutlet weak var passwordTxt: HoshiTextField!
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var emailTxt: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

    }
    
    @IBAction func SignIn(_ sender: Any) {
        signinTag = 0
        userLogIn()
     }
    
    @IBAction func SignUp(_ sender: Any) {
        print ("here")
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: SignupVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func signInWithFacebook(_ sender: Any) {
        signinTag = 1
//        facebook_login()
    }
    
    @IBAction func signInWithTwitter(_ sender: Any) {
        signinTag = 2
//        twitter_login()
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: ForgotPasswordVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    

    private func userLogIn(){
        if self.passwordTxt.text == "" || self.emailTxt.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
 
        let data : [String: String] = [
            "email": emailTxt.text!.replacedArabicDigitsWithEnglish,
            "password": passwordTxt.text!.replacedArabicDigitsWithEnglish
        ]
        
        HUD.show(.progress, onView: self.view)
        
        Alamofire.request(APIs.Instance.login(), method: .post, parameters: data , encoding: JSONEncoding.default, headers: header).responseJSON {
            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                print (APIs.Instance.login())
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print (err)
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("token",userData.Instance.token)
                        print (userData.Instance.data?.email)
                        let jwt = try decode(jwt: userData.Instance.token!)
                        //print (jwt)
                        print(jwt.body)
                        print("\\\\\\\\\\\\")
                        var dic = [String : Any]()
                        for (key, value) in jwt.body {
                            print("\(key) -> \(value)")
                            if key == "customer"{
                                print (value)
                                dic = value as! [String : Any]
                            }
                        }
                        print("\\\\\\\\\\\\")

                        print (dic)
                        print("\\\\\\\\\\\\")

                        for (key, value) in dic {
                            print("\(key) -> \(value)")
                            if key == "id"{
                                print (value)
                            UserDefaults.standard.set(value, forKey: "id")
                            }
                        }


                        print("successsss")
                        let navVC = AppStoryboard.Main.viewController(viewControllerClass: EnterWhereToGoVC.self)
                        self.navigationController?.pushViewController(navVC, animated: true)
                        
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

}


