//
//  PrivateBusVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/29/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//



    import UIKit
    
    class PrivateBusVC: UIViewController, UIPickerViewDelegate , UIPickerViewDataSource  {
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return 5
        }
        
        
        @IBOutlet weak var fromTxt: UITextField!
        @IBOutlet weak var fromCheckPointTxt: UITextField!
        @IBOutlet weak var toTxt: UITextField!
        @IBOutlet weak var toCheckPointTxt: UITextField!
        @IBOutlet weak var dateTxt: UITextField!
        
        @IBOutlet weak var timeTxt: UITextField!

        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.barTintColor = .clear
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .white

            
        }
        
}
