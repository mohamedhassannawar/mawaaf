//
//  ContactUsVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {
    
    
    @IBOutlet weak var cityLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var phoneLbl: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

    }
}
