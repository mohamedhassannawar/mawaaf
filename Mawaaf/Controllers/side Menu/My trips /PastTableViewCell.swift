//
//  PastTableViewCell.swift
//  Mawaaf
//
//  Created by Mac on 8/5/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class PastTableViewCell: UITableViewCell {

    @IBOutlet weak var sourceCityLabel : UILabel!
    @IBOutlet weak var sourceCheckPointLabel : UILabel!
    @IBOutlet weak var desCityLabel : UILabel!
    @IBOutlet weak var desCheckPointLabel : UILabel!
    @IBOutlet weak var dayLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
