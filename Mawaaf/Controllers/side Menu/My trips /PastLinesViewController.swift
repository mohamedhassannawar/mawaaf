//
//  PastLinesViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/13/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class PastLinesViewController: UIViewController {
    
    @IBOutlet weak var pastView : UIView!
    @IBOutlet weak var upcomingView : UIView!

    
    private lazy var UpcomingTripsViewController: UpcomingTripsViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "UpcomingTripsViewController") as! UpcomingTripsViewController
        self.updateContainerView(asChildViewController: viewController)
        return viewController
    }()
    private lazy var PastLinesViewController: PastLinesViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PastLinesViewController") as! PastLinesViewController
        self.updateContainerView(asChildViewController: viewController)
        return viewController
    }()

    @IBOutlet weak var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    @IBAction func upcomingButtonPressed (_ sender : Any){
        print ("upcoming")
        NotificationCenter.default.post(name: .upcoming, object: nil)

    }
    @IBAction func pastButtonPressed (_ sender : Any){
        print ("past")

    }
}

extension PastLinesViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusLineCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "PrivateBusDetailsVC")
        self.navigationController?.pushViewController(navVC!, animated: true)
    }
}
