//
//  MasterViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/13/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {
    
    private lazy var UpcomingTripsViewController: UpcomingTripsViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "UpcomingTripsViewController") as! UpcomingTripsViewController
        self.updateContainerView(asChildViewController: viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateContainerView(asChildViewController: UpcomingTripsViewController)

        NotificationCenter.default.addObserver(self, selector: #selector(upcomingTrips(notification:)), name: .upcoming, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pastTrips(notfication:)), name: .past, object: nil)
    }
    
    @objc func upcomingTrips(notification: NSNotification) {
        let vc = self.children.last
        vc!.view.removeFromSuperview()
        vc!.removeFromParent()
        let UpcomingTripsViewController = storyboard?.instantiateViewController(withIdentifier: "UpcomingTripsViewController") as! UpcomingTripsViewController
        self.addChild(UpcomingTripsViewController)
        UpcomingTripsViewController.view.frame = view.bounds
        self.view.addSubview(UpcomingTripsViewController.view)
        UpcomingTripsViewController.didMove(toParent: self)
    }
    
    @objc func pastTrips(notfication: NSNotification) {
        let vc = self.children.last
        vc!.view.removeFromSuperview()
        vc!.removeFromParent()
        
        let PastLinesViewController = storyboard?.instantiateViewController(withIdentifier: "PastLinesViewController") as! PastLinesViewController
        self.addChild(PastLinesViewController)
        PastLinesViewController.view.frame = view.bounds
        self.view.addSubview(PastLinesViewController.view)
        PastLinesViewController.didMove(toParent: self)
    }
    
}


extension Notification.Name {
    static let upcoming = Notification.Name("upcomingTrips")
    static let past = Notification.Name("pastTrips")
}
