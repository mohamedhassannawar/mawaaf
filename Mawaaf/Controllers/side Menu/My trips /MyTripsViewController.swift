//
//  MyTripsViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/13/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD


class MyTripsViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    var pastTrips = [TheData](){
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .white
        self.tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        getPastTrips()
    }
    
    private func getPastTrips(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(header)
        print(APIs.Instance.lines())
        Alamofire.request(APIs.Instance.getPastTrips(), method: .get, parameters: nil , encoding: JSONEncoding.default, headers: header).responseJSON {
            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                print (APIs.Instance.login())
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        print("success")
                        self.pastTrips = try JSONDecoder().decode([TheData].self, from: response.data!)
                        print (self.pastTrips.first?.id)
                    }catch{
                        print(error.localizedDescription)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    
    
}

extension MyTripsViewController : UITableViewDelegate , UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.pastTrips.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusLineCell", for: indexPath) as! PastTableViewCell
//            cell.dateLabel.text = "" //pastTrips[indexPath.row].trips
//            cell.dayLabel.text = ""
            cell.desCheckPointLabel.text = pastTrips[indexPath.row].destinationCheckpoint?.address ?? ""
            cell.desCityLabel.text = pastTrips[indexPath.row].destinationCheckpoint?.name ?? ""
            cell.sourceCheckPointLabel.text = pastTrips[indexPath.row].sourceCheckpoint?.address ?? ""
            cell.sourceCityLabel.text = pastTrips[indexPath.row].sourceCheckpoint?.name ?? ""
            return cell
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let navVC = AppStoryboard.Main.viewController(viewControllerClass: PrivateBusDetailsVC.self)
            navVC.index = indexPath.row
            navVC.lines = self.pastTrips
            self.navigationController?.pushViewController(navVC, animated: true)
        }

}
