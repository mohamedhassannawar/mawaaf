//
//  PrivateBusDetailsVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class PrivateBusDetailsVC: UIViewController {
    
    @IBOutlet weak var sourceCityLabel : UILabel!
    @IBOutlet weak var sourceCheckPointLabel : UILabel!
    @IBOutlet weak var desCityLabel : UILabel!
    @IBOutlet weak var desCheckPointLabel : UILabel!
    @IBOutlet weak var dayLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var arrivalDateLabel : UILabel!
    @IBOutlet weak var lineLabel : UILabel!
    @IBOutlet weak var seatsNumberLabel : UILabel!
    @IBOutlet weak var captainNameLabel : UILabel!
    @IBOutlet weak var bussNumberLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!

    var lines = [TheData]()
    var index = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // print (lines , index)
        fillData()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

    }
    
    func fillData (){
        let firstname = lines[index].trip?.captain?.coreUser?.firstName ?? ""
        let lastName = lines[index].trip?.captain?.coreUser?.lastName ?? ""
        let captainName = firstname + " " + lastName
        
        
//        self.dateLabel.text = "" //pastTrips[indexPath.row].trips
//        self.dayLabel.text = ""
        self.desCheckPointLabel.text = lines[index].destinationCheckpoint?.address ?? ""
        self.desCityLabel.text = lines[index].destinationCheckpoint?.name ?? ""
        self.sourceCheckPointLabel.text = lines[index].sourceCheckpoint?.address ?? ""
        self.sourceCityLabel.text = lines[index].sourceCheckpoint?.name ?? ""
       // self.arrivalDateLabel.text = "" //lines[index]
        self.captainNameLabel.text = captainName
        self.bussNumberLabel.text = lines[index].trip?.captain?.car?.plateNumber ?? ""
        if let lineNumber = lines[index].trip?.line?.id {
            self.lineLabel.text = "#\(lineNumber)"
        }
        if let price = lines[index].price {
            self.priceLabel.text = "\(price)"
        }
        if let seatsNumber = lines[index].seatsNumber {
            self.seatsNumberLabel.text = "\(seatsNumber)"
        }
    }
    
    @IBAction func needHelpButtonPressed ( _ sender : Any ){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: SendMessageViewController.self)
        navVC.index = self.index
        navVC.lines = self.lines
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
}
