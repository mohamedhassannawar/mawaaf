//
//  SideMenuViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/12/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import JWTDecode
//import SwiftJWT


class SideMenuViewController: UIViewController {

    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var emailLabel : UILabel!

 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        getDataFromtoken()

    }
    
    fileprivate func getDataFromtoken() {

        guard let userToken = userData.Instance.token else {
            self.makeDoneAlert(title: "error", SubTitle: "SomeThing Went wrong", Image: UIImage())
            return
        }

        do{
            print (userToken)

            let jwt = try decode(jwt: userToken)
            print (jwt)


            let claimUserName = jwt.claim(name: "username")
            let claimID = jwt.claim(name: "id")
            if let username = claimID.string {
                print("username in jwt was \(username)")
            }

            if let username = claimUserName.string {
                print("username in jwt was \(username)")
                userNameLabel.text = username
            }
        }catch{
           self.makeDoneAlert(title: "error", SubTitle: "SomeThing Went wrong", Image: UIImage())
        }
    }
    
    @IBAction func myTripsButtonPressed (_ sender : Any){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: MyTripsViewController.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func promoCodesButtonPressed (_ sender : Any){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: PromoCodesViewController.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func contactUsButtonPressed (_ sender : Any){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: ContactUsVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func privacyButtonPressed (_ sender : Any){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: Privacy_PolicyVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func needHelpButtonPressed (_ sender : Any){
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: ChooseYorTripVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func languageButtonPressed (_ sender : Any){


    }
    
    @IBAction func LogoutButtonPressed (_ sender : Any){
        userData.Instance.remove()
        userData.Instance.fetchUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navVc = storyboard.instantiateViewController(withIdentifier: "loginNavigation")
        
        self.dismiss(animated: true) { () -> Void in
            UIApplication.shared.keyWindow?.rootViewController = navVc
        }
    }

}
