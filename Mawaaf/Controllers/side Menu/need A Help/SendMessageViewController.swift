//
//  SendMessageViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/13/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class SendMessageViewController: UIViewController {
    
    @IBOutlet weak var sourceCityLabel : UILabel!
    @IBOutlet weak var sourceCheckPointLabel : UILabel!
    @IBOutlet weak var desCityLabel : UILabel!
    @IBOutlet weak var desCheckPointLabel : UILabel!
    @IBOutlet weak var dayLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var messageTextView : UITextView!

    var lines = [TheData]()
    var index = -1
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.dateLabel.text = "" //pastTrips[indexPath.row].trips
//        self.dayLabel.text = ""
        self.desCheckPointLabel.text = lines.first?.destinationCheckpoint?.address ?? ""
        self.desCityLabel.text = lines.first?.destinationCheckpoint?.name ?? ""
        self.sourceCheckPointLabel.text = lines.first?.sourceCheckpoint?.address ?? ""
        self.sourceCityLabel.text = lines.first?.sourceCheckpoint?.name ?? ""
        self.navigationController?.navigationBar.tintColor = .white
    }
    @IBAction func sendPressed (_ sender : Any){
        if self.messageTextView.text == "" || self.messageTextView.text == "" {
            HUD.flash(.label(NSLocalizedString("please Enter Your message", comment: "")), delay: 1.0)
            return
        }

        sendMessage()
    }
    private func sendMessage(){

        let header = APIs.Instance.getHeader()
        
        let data : [String: Any] = [
            "details": messageTextView.text.replacedArabicDigitsWithEnglish,
            "request": [
                "id": self.lines.first?.trip?.id!
            ]
        ]
        
        print (data)
        HUD.show(.progress, onView: self.view)
        
        Alamofire.request(APIs.Instance.sendMessage(), method: .post, parameters: data , encoding: JSONEncoding.default, headers: header).responseJSON {
            (response:DataResponse) in
            print ("response is ")
            print(response)
            switch(response.result) {
            case .success(let value):
                print (APIs.Instance.login())
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.detail ?? "", Image:  UIImage())
                        print (err)
                        print(err.detail)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        print("success")
                        self.makeDoneAlert(title: "Thanks", SubTitle: "your message has been sent .. Thanks for your feedback", Image: UIImage())
                        self.navigationController?.popViewController(animated: true)
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

}
