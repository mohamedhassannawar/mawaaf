//
//  AddPromoCodeVC.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//



    import UIKit
    import Alamofire
    import PKHUD
    import FCAlertView
    class AddPromoCodeVC: UIViewController {
        
        //@IBOutlet var backGround: UIView!
        override func viewDidLoad() {
            super.viewDidLoad()
//            let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
//            mytapGestureRecognizer.numberOfTapsRequired = 1
//            self.backGround.addGestureRecognizer(mytapGestureRecognizer)
//            userData.Instance.fetchUser()
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.barTintColor = .clear
            self.navigationController?.navigationBar.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .white

        }
        
        @objc func myTapAction(recognizer: UITapGestureRecognizer) {
            self.dismiss(animated: true, completion: nil)
            
        }
        
        @IBAction func cancelButtonPressed(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        @IBAction func addButtonPressed(_ sender: Any) {
           let Vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PromoCodesViewController") as! PromoCodesViewController
            //Vc.flag = 1
            self.navigationController?.popViewController(animated: true)

        }
}
