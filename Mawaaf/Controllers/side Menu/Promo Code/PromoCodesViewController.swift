//
//  PromoCodesViewController.swift
//  Mawaaf
//
//  Created by Mac on 7/13/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit

class PromoCodesViewController: UIViewController {
    
    @IBOutlet weak var validPromoCodeStackView : UIStackView!
    @IBOutlet weak var noPromoLabel : UILabel!
    var flag = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    override func viewWillAppear(_ animated: Bool) {
        print ("hi")
        if flag == 1 {
            print ("hi 1")

            let Vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuccessfulAddVC")
            Vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(Vc, animated: true, completion: nil)
        }
    }

    @IBAction func addPromoCodeButtonPressed ( _ sender : Any){

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPromoCodeVC")
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
