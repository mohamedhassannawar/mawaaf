//
//  UserSendReviewToTrip.swift
//  Mawaaf
//
//  Created by Mohamed Nawar on 6/30/19.
//  Copyright © 2019 Mohamed Nawar. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
class UserSendReviewToTrip: UIViewController , UITextFieldDelegate ,  UITextViewDelegate {
    
    @IBOutlet var backGround: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var rattingView: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer.numberOfTapsRequired = 1
        self.backGround.addGestureRecognizer(mytapGestureRecognizer)
        userData.Instance.fetchUser()
        rattingView.value = CGFloat(0)
        messageTextView.delegate = self
        messageTextView.text = NSLocalizedString("Write Comment", comment: "")
        messageTextView.textColor = UIColor.lightGray
        self.title = NSLocalizedString("Send Review", comment:"أرسال تقييم")
        
    
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white

    }
    
    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 1, green: 0.4, blue: 0, alpha: 1)
        textViewView.borderWidth = 1
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 1, green: 0.4, blue: 0, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendMessage(_ sender: Any) {
        
    }
}
